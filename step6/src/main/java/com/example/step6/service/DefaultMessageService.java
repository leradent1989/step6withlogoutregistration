package com.example.step6.service;

import com.example.step6.dao.MessageDao;
import com.example.step6.domain.Message;
import com.example.step6.dao.JdbcMessageDao;

import java.util.List;

public class DefaultMessageService implements MessageService  {
    MessageDao jdbcMessageDao;

    public DefaultMessageService(JdbcMessageDao jdbcMessageDao) {
        this.jdbcMessageDao = jdbcMessageDao;
    }
    public List <Message> findAll(){
        return  jdbcMessageDao.findAll();
    }
    public List <Message> findByProfile(Long id){
        return  jdbcMessageDao.findByProfile(id);
    }

    public  void addNewMessage (Long id,Message message){
        jdbcMessageDao.addNewMessage(id,message);
    }
}
