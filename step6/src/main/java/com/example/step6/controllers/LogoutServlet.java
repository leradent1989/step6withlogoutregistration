package com.example.step6.controllers;

import com.example.step6.domain.TemplateEngine;
import com.example.step6.service.CookieUtil;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;



public class LogoutServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    public LogoutServlet(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        CookieUtil.findCookieByName(request,"userId")
                .ifPresent(c -> {
                    c.setMaxAge(0);
                    c.setPath("/");
                    response.addCookie(c);
                });


        response.sendRedirect("/login");
    }
}
