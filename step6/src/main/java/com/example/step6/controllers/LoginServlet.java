package com.example.step6.controllers;

import com.example.step6.domain.Profile;
import com.example.step6.domain.TemplateEngine;
import com.example.step6.service.ProfileService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet  extends HttpServlet {

    TemplateEngine templateEngine;

    ProfileService profileService ;

    public LoginServlet(TemplateEngine templateEngine,ProfileService profileService) {
        this.templateEngine = templateEngine;
        this.profileService = profileService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      templateEngine.render("login.html",resp);
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String login = req.getParameter("login");
        String password = req.getParameter("password");
       Profile user = profileService.findByLoginPass(login,password);

      System.out.println(user);
      if(user != null){

          Cookie cookie = new Cookie("userId",String.valueOf(user.getId()));
          resp.addCookie(cookie);

          resp.sendRedirect("/users");


        }else{

            resp.sendRedirect("/login");
        }


    }


}
