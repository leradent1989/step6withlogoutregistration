package com.example.step6.controllers;

import com.example.step6.domain.TemplateEngine;
import com.example.step6.service.DefaultLikedService;
import com.example.step6.service.LikedService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class LikedServlet extends HttpServlet {
    public TemplateEngine templateEngine;

   LikedService defaultLikedService;
    public LikedServlet(TemplateEngine templateEngine,DefaultLikedService defaultLikedService) {

        this.templateEngine = templateEngine;
        this.defaultLikedService =defaultLikedService;
    }



    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


      /*  resp.setContentType("text/html");
        resp.getWriter().println("""
                   
                    <h2  style:"margin:0 auto;color:grey">Liked profiles</h2>
                    
                """);*/

        if (defaultLikedService.findAll().size() == 0) {
            resp.setContentType("text/html");
            resp.getWriter().println("""
                       <div style="width =500px;height:500px; display:flex;align-items:center;flex-direction:column" >
                        <h2>There is no liked profiles at the moment</h2>
                        <a style="display:block;font-size:25px;" href="/users">View profiles</a>
                        </div>
                    """);

        } else {


            Map<String, Object> params = Map.of(

                    "profiles", defaultLikedService.findAll()
            );

            templateEngine.render("liked.html", params, resp);
        }
    }


}
