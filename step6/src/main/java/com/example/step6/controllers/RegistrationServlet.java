package com.example.step6.controllers;

import com.example.step6.domain.Message;
import com.example.step6.domain.Profile;
import com.example.step6.domain.TemplateEngine;
import com.example.step6.service.MessageService;
import com.example.step6.service.ProfileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RegistrationServlet extends HttpServlet {
    private ProfileService profileService;


    private TemplateEngine templateEngine;

    public RegistrationServlet() {}

    public RegistrationServlet(ProfileService profileService, TemplateEngine templateEngine) {
        this.profileService = profileService;
        this.templateEngine = templateEngine;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> params = new HashMap<>();
        params.put("submitMapping", "/registration");
        templateEngine.render("registration.ftl", params, resp );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String imgUrl = "https://www.meme-arsenal.com/memes/77086a4ce9dcc164c77db76c7853f012.jpg";
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        Long id = (long) (profileService.findAll().size() +1);


        Profile user = new Profile(id,name,surname,imgUrl,login,password);
        profileService.addNewProfile(user);
        resp.sendRedirect("/login");

    }
}
