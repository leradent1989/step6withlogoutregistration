package com.example.step6.service;

import com.example.step6.domain.Profile;
import com.example.step6.dao.LikedDao;

import java.util.List;

public class DefaultLikedService implements LikedService{
   private LikedDao likedDao;

    public DefaultLikedService   (LikedDao likedDao) {
        this.likedDao = likedDao;
    }

    public List<Profile> findAll(){
        return  likedDao.findAll();
    }

    public  void addToList(Profile profile){
        likedDao.addToList(profile);
    }
}
