package com.example.step6.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import com.example.step6.domain.Profile;
import org.postgresql.ds.PGPoolingDataSource;
interface ProfileDao {

     List<Profile> findAll();
     List <Profile>  findByIndex(Long index);

    Profile findByLoginPass(String loginParam, String passwordParam);
    void addNewProfile(Profile profile);
      default Connection  getConnection()throws SQLException {
     PGPoolingDataSource source = new PGPoolingDataSource();

        source.setServerName("localhost:5432");
        source.setDatabaseName("tinderapp");
        source.setUser("postgres");
        source.setPassword("root");
        source.setMaxConnections(10);
         source.getConnection();
        return source.getConnection();
        //Підключення до віддаленої бази даних
//return DriverManager.getConnection("jdbc:postgresql://dumbo.db.elephantsql.com:5432/rehkghpc", "rehkghpc", "WUBzwzbwEL1BqcNut2F4qvbvLxjngI4F");

    }
}
