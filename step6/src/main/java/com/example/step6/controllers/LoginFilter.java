package com.example.step6.controllers;

import com.example.step6.domain.Profile;
import com.example.step6.service.DefaultProfileService;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Optional;

public class LoginFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig)
            throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws java.io.IOException,
            ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;

        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        Cookie[] cookies = httpServletRequest.getCookies();
        Optional<Cookie> userCookie = cookies != null
                ? Arrays.stream(cookies).filter(cookie -> cookie.getName().equals("userId")).findFirst()
                : Optional.empty();
        if (!userCookie.isPresent()) {


          httpServletRequest.getRequestDispatcher("/login").forward(request,response);
    }
    else{
           chain.doFilter(request,response);

    }

}

    @Override
    public void destroy(){}

}